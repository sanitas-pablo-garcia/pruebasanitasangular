'use strict';

const fs = require('fs');
const lipsum = require('lorem-ipsum');

/* Constants */
const IMAGE_URL = 'https://picsum.photos/id/';
const imageWidth = 500;
const imageHeight = 500;
const lIpsumGenerator = new lipsum.LoremIpsum({
    sentencesPerParagraph: {
        max: 8,
        min: 4
    },
});


let image = {};
let images = [];

for (let i = 1; i <= 4000; i++) {
    image = {
        id: i,
        photo: IMAGE_URL + i + '/' + imageWidth + '/' + imageHeight,
        text: lIpsumGenerator.generateParagraphs(2)

    };
    images.push(image);
}

console.log('\nGenerando archivo...');
let data = JSON.stringify(images);
fs.writeFileSync('src/assets/images.json', JSON.stringify(images),
    (err) => {
        if (err) { throw err; }
    },
    () => console.log('\nArchivo de imágenes generado.'));
