
import { HttpClient } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';

import { FileService } from './file.service';
import * as imagesJSONPath from '../../assets/images-20.json';
import { IMAGES } from '../../../server/db-data';
import { config } from '../config/app.config';


describe('FileService', () => {
  let fileService: FileService;
  let httpTestingController: HttpTestingController;
  let imagesFileName = config.imagesFileName;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ],
      providers: [
        FileService
      ]
    });

    fileService = TestBed.get(FileService);
    httpTestingController = TestBed.get(HttpTestingController);
  });

  it('should find the images json file', () => {
    expect(imagesJSONPath).toBeTruthy();
  });

  it('should retrieve all images', () => {
    fileService.getImages().subscribe(
      images => {
        expect(images).toBeTruthy('No images returned');
      });

    const req = httpTestingController.expectOne(imagesFileName);
    expect(req.request.method).toEqual('GET');

    req.flush({ payload: Object.values(IMAGES) });
  });
});
