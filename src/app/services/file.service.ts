import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';

import { config } from '../config/app.config';
import { Image } from '../interfaces/image';

/**
 * File service
 */
@Injectable({
  providedIn: 'root'
})

export class FileService {

  /** filename of json file */
  imagesFileName: string;

  /** class constructor */
  constructor(private http: HttpClient) {
    this.imagesFileName = config.imagesFileName;
  }

  /** async get images method */
  public getImages(): Observable<Image[]> {
    return this.http.get<Image[]>(this.imagesFileName)
      .pipe(
        catchError(error => {
          console.log('El archivo de imágenes no se encuentra en la ruta especificada.' + error);
          return throwError(error);
        })
      );
  }
}
