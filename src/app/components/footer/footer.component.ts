import { Component, OnInit } from '@angular/core';
import { config } from '../../config/app.config';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

  date: Date = new Date();
  signText: string = config.signText;
  constructor() {}

  ngOnInit(): void {}

}
