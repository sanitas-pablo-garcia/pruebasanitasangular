import { IMAGES } from '../../../../server/db-data';
import { Image } from 'src/app/interfaces/image';


export function setupImages(): Image[] {
  return Object.values(IMAGES) as Image[];
}


