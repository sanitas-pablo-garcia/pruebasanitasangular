import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Image } from 'src/app/interfaces/image';
import { FileService } from 'src/app/services/file.service';

/**
 * Home page component
 */
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})

export class HomeComponent implements OnInit {

  /** search criteria for filtering images */
  searchText = '';

  /** images array */
  images: Image[];

  /** class constructor */
  constructor(private fileService: FileService,
              private router: Router) { }

  /** oninit lifecycle method */
  ngOnInit(): void {
    this.fileService.getImages().subscribe(
      results => this.images = results,
      () => this.router.navigateByUrl('error404')
    );
  }
}
