import { CommonModule } from '@angular/common';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { CUSTOM_ELEMENTS_SCHEMA, DebugElement } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { By } from "@angular/platform-browser";
import { of } from 'rxjs';
import { FilterPipe } from 'src/app/pipes/filter.pipe';
import { FileService } from 'src/app/services/file.service';
import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home.component';
import { setupImages } from './setup-test-data';

describe('HomeComponent', () => {
  let fileService: any;
  let component: HomeComponent;
  let fixture: ComponentFixture<HomeComponent>;
  let el: DebugElement;
  const allImages = setupImages();

  beforeEach(async(() => {
    const fileServiceSpy = jasmine.createSpyObj('FileService', ['getImages']);

    TestBed.configureTestingModule({
      declarations: [HomeComponent, FilterPipe],
      imports: [
        CommonModule,
        HomeRoutingModule,
        HttpClientModule,
        RouterTestingModule,
        FormsModule
      ],
      providers: [
        { provide: FileService, useValue: fileServiceSpy }
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
      .compileComponents().then(
        () => {
          fixture = TestBed.createComponent(HomeComponent);
          component = fixture.componentInstance;
          el = fixture.debugElement;

          fileService = TestBed.get(FileService);
        }
      );
  }));

  it('should display all images', () => {
    fileService.getImages.and.returnValue(of(allImages));

    fixture.detectChanges();

    const images = el.queryAll(By.css('img'));

    expect(images.length).toBeGreaterThan(0, 'Unexpected number of images found');
  });
});
