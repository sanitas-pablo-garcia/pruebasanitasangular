import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Error404PageComponent } from './error404-page.component';

describe('Error404Page', () => {
  let component: Error404PageComponent;
  let fixture: ComponentFixture<Error404PageComponent>;

  beforeEach(() => {
    fixture = TestBed.createComponent(Error404PageComponent);
    component = fixture.debugElement.componentInstance;
    fixture.detectChanges();
  });

});
