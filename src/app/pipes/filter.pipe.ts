import { Pipe, PipeTransform } from '@angular/core';

/**
 * Text filter pipe
 */
@Pipe({ name: 'filterText' })
export class FilterPipe implements PipeTransform {
  /**
   * Transform method
   */
  transform(items: any[], searchText: string): any[] {
    if (!items) {
      return [];
    }
    if (!searchText) {
      return items;
    }
    searchText = searchText.toLocaleLowerCase();

    return items.filter(item => {
      return item.id === searchText || item.text.toLocaleLowerCase().includes(searchText);
    });
  }
}
