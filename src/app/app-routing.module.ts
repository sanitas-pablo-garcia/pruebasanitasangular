import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { Error404PageComponent } from './pages/error404-page/error404-page.component';

const routes: Routes = [
  // ruta por defecto
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'error404', component: Error404PageComponent },
  { path: 'home', loadChildren: () => import('./pages/home/home.module').then(m => m.HomeModule) },
  // cualquier otra ruta no válida, error 404
  { path: '**', redirectTo: 'error404' }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      initialNavigation: 'enabled',
      scrollPositionRestoration: 'enabled',
      anchorScrolling: 'enabled'
    })
  ],
  exports: [
    RouterModule
  ]
})

export class AppRoutingModule {}
