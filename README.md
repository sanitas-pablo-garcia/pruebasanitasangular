# PruebaSanitas

Este proyecto se ha generado con [Angular CLI](https://github.com/angular/angular-cli) version 9.1.11.

## Requisitos previos

Ejecutar `npm install` para instalar todos los paquetes y sus dependencias.

Una vez instalados todos los paquetes, podemos ejecutar el siguiente comando para la generación del archivo JSON que contendrá las 4.000 imágenes. En caso de existir (en la carpeta /src/assets/), se sobrescribirá:

`npm run generate-images`

## Ejecutar la app 

Para lanzar la aplicación, el comando necesario es 

`ng serve`

 Dispondremos de la app en ejecución en la siguiente URL:
 
`http://localhost:4200/`

El servidor se mantendrá a la escucha, de tal forma que si modificamos el código de la app, se recargará sólo con los nuevos cambios.

## Code scaffolding

Ejecuta `ng generate component component-name` para generar un nuevo componente.

Para la generación de otras instancias, se pueden usar los siguientes comandos:

 `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
